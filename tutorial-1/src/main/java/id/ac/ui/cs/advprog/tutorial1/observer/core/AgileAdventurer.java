package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }
    public void update() {
        String str = this.guild.getQuestType();
        if (str.equals("D") || str.equals("R")) {
            Quest q = this.guild.getQuest();
            this.getQuests().add(q);
        }
    }
}
