package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
    }
    public void update() {
        Quest q = this.guild.getQuest();
        this.getQuests().add(q);
    }

}
