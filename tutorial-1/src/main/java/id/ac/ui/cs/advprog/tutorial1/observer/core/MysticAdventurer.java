package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }
    public void update() {
        String str = this.guild.getQuestType();
        if (str.equals("D") || str.equals("E")) {
            Quest q = this.guild.getQuest();
            this.getQuests().add(q);
        }
    }

}
