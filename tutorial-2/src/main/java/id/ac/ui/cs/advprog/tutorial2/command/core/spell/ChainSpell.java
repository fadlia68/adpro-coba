package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {

    public ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    public void cast() {
        for (int i = 0; i < this.spells.size(); i++) {
            this.spells.get(i).cast();
        }
    }

    public void undo() {
        for (int i = this.spells.size() - 1; i >= 0; i--) {
            this.spells.get(i).cast();
        }
    }
}
