package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {

    public HighSpiritStealthSpell(HighSpirit spirit) {
        super(spirit);
    }

    public String spellName() {
        return spirit.getRace() + ":Stealth";
    }

    public void cast() {
        this.spirit.stealthStance();
    }
}
